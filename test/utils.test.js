// import { toKebabCase } from "../lib/utils";
const utils = require('../lib/utils');

const { toKebabCase } = utils;

describe('String kebab-case parser', () => {
  it('correctly parses regular string', () => {
    const str1 = 'Section Title Easy';
    const str2 = 'Section Title,        !NotEasy#! ""()';
    const str3 = 'section TiTle bAdTyped>>(extra)';
    expect(toKebabCase(str1)).toBe('section-title-easy');
    expect(toKebabCase(str2)).toBe('section-title-not-easy');
    expect(toKebabCase(str3)).toBe('section-ti-tle-b-ad-typed-extra');
  });
});
